require 'test_helper'

class FriendsConnectionsControllerTest < ActionController::TestCase
  setup do
    @friends_connection = friends_connections(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:friends_connections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create friends_connection" do
    assert_difference('FriendsConnection.count') do
      post :create, friends_connection: @friends_connection.attributes
    end

    assert_redirected_to friends_connection_path(assigns(:friends_connection))
  end

  test "should show friends_connection" do
    get :show, id: @friends_connection
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @friends_connection
    assert_response :success
  end

  test "should update friends_connection" do
    put :update, id: @friends_connection, friends_connection: @friends_connection.attributes
    assert_redirected_to friends_connection_path(assigns(:friends_connection))
  end

  test "should destroy friends_connection" do
    assert_difference('FriendsConnection.count', -1) do
      delete :destroy, id: @friends_connection
    end

    assert_redirected_to friends_connections_path
  end
end
