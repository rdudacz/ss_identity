require 'test_helper'

class WorkPositionsControllerTest < ActionController::TestCase
  setup do
    @work_position = work_positions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:work_positions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create work_position" do
    assert_difference('WorkPosition.count') do
      post :create, work_position: @work_position.attributes
    end

    assert_redirected_to work_position_path(assigns(:work_position))
  end

  test "should show work_position" do
    get :show, id: @work_position
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @work_position
    assert_response :success
  end

  test "should update work_position" do
    put :update, id: @work_position, work_position: @work_position.attributes
    assert_redirected_to work_position_path(assigns(:work_position))
  end

  test "should destroy work_position" do
    assert_difference('WorkPosition.count', -1) do
      delete :destroy, id: @work_position
    end

    assert_redirected_to work_positions_path
  end
end
