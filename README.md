# README file
Setup:

1. git clone git@bitbucket.org:rdudacz/ss_identity.git
2. cd ss_identity
3. set up your credentials in config/database.yml
4. bundle install
5. rake db:create
6. rake db:migrate
7. thin start -p 3002 (callbacks in Facebook & LinkedIn API are set to the port number 3002)
8. log-in via your Facebook account
9. log-in via your LinkedIn account
10. see the stats