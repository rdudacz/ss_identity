class FriendsConnectionsController < ApplicationController
  # GET /friends_connections
  # GET /friends_connections.json
  def index
    @friends_connections = FriendsConnection.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @friends_connections }
    end
  end

  # GET /friends_connections/1
  # GET /friends_connections/1.json
  def show
    @friends_connection = FriendsConnection.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @friends_connection }
    end
  end

  # GET /friends_connections/new
  # GET /friends_connections/new.json
  def new
    @friends_connection = FriendsConnection.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @friends_connection }
    end
  end

  # GET /friends_connections/1/edit
  def edit
    @friends_connection = FriendsConnection.find(params[:id])
  end

  # POST /friends_connections
  # POST /friends_connections.json
  def create
    @friends_connection = FriendsConnection.new(params[:friends_connection])

    respond_to do |format|
      if @friends_connection.save
        format.html { redirect_to @friends_connection, notice: 'Friends connection was successfully created.' }
        format.json { render json: @friends_connection, status: :created, location: @friends_connection }
      else
        format.html { render action: "new" }
        format.json { render json: @friends_connection.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /friends_connections/1
  # PUT /friends_connections/1.json
  def update
    @friends_connection = FriendsConnection.find(params[:id])

    respond_to do |format|
      if @friends_connection.update_attributes(params[:friends_connection])
        format.html { redirect_to @friends_connection, notice: 'Friends connection was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @friends_connection.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /friends_connections/1
  # DELETE /friends_connections/1.json
  def destroy
    @friends_connection = FriendsConnection.find(params[:id])
    @friends_connection.destroy

    respond_to do |format|
      format.html { redirect_to friends_connections_url }
      format.json { head :no_content }
    end
  end
end
