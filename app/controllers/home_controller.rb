class HomeController < ApplicationController
  def index
    unless current_user.nil? #if the user is logged-in first time, save all available informations via API
      FriendsConnection.create_new(current_user) unless FriendsConnection.find_by_user_id(current_user.id)
    end
    @users_lin = User.find_users('linkedin')
    @users_fb = User.find_users('facebook')
  end
end
