class WorkPositionsController < ApplicationController
  # GET /work_positions
  # GET /work_positions.json
  def index
    @work_positions = WorkPosition.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @work_positions }
    end
  end

  # GET /work_positions/1
  # GET /work_positions/1.json
  def show
    @work_position = WorkPosition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @work_position }
    end
  end

  # GET /work_positions/new
  # GET /work_positions/new.json
  def new
    @work_position = WorkPosition.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @work_position }
    end
  end

  # GET /work_positions/1/edit
  def edit
    @work_position = WorkPosition.find(params[:id])
  end

  # POST /work_positions
  # POST /work_positions.json
  def create
    @work_position = WorkPosition.new(params[:work_position])

    respond_to do |format|
      if @work_position.save
        format.html { redirect_to @work_position, notice: 'Work position was successfully created.' }
        format.json { render json: @work_position, status: :created, location: @work_position }
      else
        format.html { render action: "new" }
        format.json { render json: @work_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /work_positions/1
  # PUT /work_positions/1.json
  def update
    @work_position = WorkPosition.find(params[:id])

    respond_to do |format|
      if @work_position.update_attributes(params[:work_position])
        format.html { redirect_to @work_position, notice: 'Work position was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @work_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_positions/1
  # DELETE /work_positions/1.json
  def destroy
    @work_position = WorkPosition.find(params[:id])
    @work_position.destroy

    respond_to do |format|
      format.html { redirect_to work_positions_url }
      format.json { head :no_content }
    end
  end
end
