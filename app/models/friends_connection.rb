class FriendsConnection < ActiveRecord::Base
  belongs_to :user, :foreign_key => 'user_id', :class_name => 'User'
  belongs_to :user_friend, :foreign_key => 'friend_with', :class_name => 'User'
  has_many :work_positions, :foreign_key => 'friends_connection_uid'
  
  #save user's friends/connections - this method is used already first time, when is user logged in
  def self.create_new(cur_user)
    case cur_user.provider
      when 'facebook'        
        facebook_user = FbGraph::User.new(cur_user.facebook_uid, :access_token => cur_user.oauth_token).fetch

          facebook_user.friends.each do |friend| #save friends of user, who is logged-in
            begin
              remote_profile = FbGraph::User.fetch(friend.identifier, :access_token => cur_user.oauth_token)
          
              saved_fr = User.create_fb_friend(cur_user, remote_profile)
              #saving "my friends" - association
              self.create({
                :user_id => cur_user.id,
                :friend_with => saved_fr.id
              })

              #save friends work history
              remote_profile.work.each do |work_pos|
                WorkPosition.create({
                  :user_id => saved_fr.id,
                  :position => work_pos.employer.name
                })
              end
            
              #save friends work history  
              remote_profile.education.each do |edu|
                Education.create({              
                  :user_id => saved_fr.id,
                  :school => edu.school.name.downcase,
                  :start_year => nil,
                  :end_year => nil
                })
              end  
            rescue => e #handling exception
              puts e.message  
            end #exception  
          end
        
        WorkPosition.my_create_new(cur_user) #save work history of user, who is logged-in
        Education.my_create_new(cur_user) #save education history of the user, who is logged-in
        
      when 'linkedin'
        linkedin = LinkedIn::Client.new('ti57m4brxgf2', 'FqPh4FLHr2C0e3gh') 
        linkedin.authorize_from_access(cur_user.oauth_token, cur_user.oauth_secret)
        lin_user = linkedin.profile(:id => cur_user.linkedin_uid, :fields => [:date_of_birth, :main_address, :connections])
        
        User.update_all({:birth => (User.linkedin_birth(lin_user.date_of_birth) if lin_user.date_of_birth),
                         :location => lin_user.main_address},
                        {:id => cur_user.id})

        lin_user.connections.all.each do |item| #save connections of user, who is logged-in
          lin_conn = linkedin.profile(:id => item.id, :fields => [:id, :first_name, :last_name, :date_of_birth, :main_address, :public_profile_url, :educations, :positions])

          saved_conn = User.create_lin_friend(cur_user, lin_conn)

          #saving "my friends" - association
          self.create({
            :user_id => cur_user.id,
            :friend_with => saved_conn.id
          })
          
          #save connection's work position
          lin_conn.educations.all.each do |edu|
            Education.create({
              :user_id => saved_conn.id,
              :school => edu.school_name.downcase,
              :start_year => nil,
              :end_year => nil
            })
          end
          
          #save connection's work position
          lin_conn.positions.all.each do |work|
            WorkPosition.create({
              :user_id => saved_conn.id,
              :position => work.company.name.downcase
            })
          end
        end
        WorkPosition.my_create_new(cur_user) #save work history of the user, who is logged-in
        Education.my_create_new(cur_user) #save education history of the user, who is logged-in
    end
  end
  
  def self.find_mutual_friend(fb_user, lin_friend)
    find_friend = self.where('first_name = ? AND last_name = ? AND user_id = ?', lin_friend.first_name, lin_friend.last_name, fb_user.id).first
    return find_friend
  end
end
