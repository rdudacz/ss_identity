class WorkPosition < ActiveRecord::Base
  belongs_to :friends_connection, :foreign_key => 'friends_connection_uid'
  belongs_to :user

  #method for saving work history of user, who is log-in
  def self.my_create_new(cur_user)
    case cur_user.provider
      when 'facebook' 
        my_account = FbGraph::User.new(cur_user.facebook_uid, :access_token => cur_user.oauth_token).fetch
        my_account.work.each do |position|
          self.create({
            :user_id => cur_user.id,
            :position => position.employer.name.downcase
          })
        end
        
      when 'linkedin'
        linkedin = LinkedIn::Client.new('ti57m4brxgf2', 'FqPh4FLHr2C0e3gh') 
        linkedin.authorize_from_access(cur_user.oauth_token, cur_user.oauth_secret)
        user = linkedin.profile(:fields => %w(positions))
        user.positions.all.each do |work|
          self.create({
            :user_id => cur_user.id,
            :position => work.company.name.downcase
          })
        end      
    end
  
  end
end
