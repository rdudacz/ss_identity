class Match < ActiveRecord::Base
  #we're starting the matching by user's first + last name
  def self.check_names(fb_user, show_page = nil)
    res_lin_users = User.where('first_name = ? AND last_name = ? AND facebook_uid IS NULL', fb_user.first_name, fb_user.last_name)
    
    if res_lin_users.count == 0 # Michael Novak vs. Mike Novak
      res_lin_users = User.where('last_name = ? AND facebook_uid IS NULL', fb_user.last_name)
      return self.check_birthdays(fb_user, res_lin_users)
    end
    
    #take a look what we found
    self.count_of_results(fb_user, res_lin_users)
  end
  
  #comparing users' birthdays
  def self.check_birthdays(fb_user, linkedin_users)
    linkedin_users.each do |item|
      if item.birth == fb_user.birth #the same birthdays, we found the needed perosn
        return item.linkedin_uid   
      end
    end 
    return self.check_usernames(fb_user, linkedin_users) #we didn't find the guy by comparing possible birthdays => go to compare usernames
  end
  
  #params: FB user, who we try to map and list of possible LinkedIns users (with the same first+last name)
  def self.check_usernames(fb_user, linkedin_users)
    linkedin_users.each do |item|
      if item.username == fb_user.username #the same usernames, we found the people
        return item.linkedin_uid   
      end
    end
    self.check_work_positions(fb_user, linkedin_users) #we didn't find the guy by comparing possible usernames => go to compare users's work positions
  end  

  #get the count of results 
  def self.count_of_results(fb_usr, results)
    if results.count == 1 #just one found row => we're done
      results.first.linkedin_uid
    elsif results.count == 0 #nothing found => return nil    
      nil
    else #found more rows - go to compare usernames
      self.check_birthdays(fb_usr, results)
    end
  end
  
  #comparing based on user's work positions
  def self.check_work_positions(fb_user, linkedin_users)
    fb_work_pos = WorkPosition.where('user_id = ? AND friends_connection_uid IS NULL', fb_user.id) #grab all FB user's work positions
    if fb_work_pos.count > 0   
      fb_work_pos.each do |fb_work|
        fb_position = fb_work.position
        
        linkedin_users.each do |lin_work| #go through all possible LinkedIns users
          lin_found_work = WorkPosition.find_by_user_id(lin_work.id) #get LinkedIn's user 'headline'
          if lin_found_work
            return lin_work.linkedin_uid if fb_position == lin_found_work.position #and if we've the same, then done
          end
        end
      end #end of fb_work_pos.each
    end
    self.check_educations(fb_user, linkedin_users) #we didn't find the guy by comparing based on work positions => go to compare users's educations 
  end
  
  #comparing based on user's education
  def self.check_educations(fb_user, linkedin_users)
    fb_edus = Education.where('user_id = ?', fb_user.id)
    if fb_edus.count > 0
      fb_edus.each do |fb_edu|
        fb_school = fb_edu.school
      
        linkedin_users.each do |lin_user|
          lin_user.educations.each do |lin_edu|
            if lin_edu
              return lin_user.linkedin_uid if fb_school == lin_edu.school
            end
          end
        end
      
      end #end of fb_edus.each
    end  
    self.check_locations(fb_user, linkedin_users)
  end
  
  #comparing based on user's location
  def self.check_locations(fb_user, linkedin_users)
    linkedin_users.each do |item|
      unless item.location.blank? #compare locations just if exist  
        return item.linkedin_uid if item.location == fb_user.location #the same locations, we found the needed perosn
      end
    end 
    #self.check_mutuals(fb_user, linkedin_users)
    nil
  end
  
  #mapping the person based on the mutual friends
  def self.check_mutuals(fb_user, linkedin_users)
    mutuals = 0
    linkedin_users.each do |lin_user|
      lin_user.friends_connections.each do |lin_friend|
        found = FriendsConnection.find_mutual_friend(fb_user, lin_friend)
        mutuals += 1 if found
        return lin_user.linkedin_uid if mutuals == 3 #if we found 3 mutual persons, we match this 'lin_friend' person and 'fb_user' as the same one
      end #end of lin_user.friends_connections
    end #end of linkedin_users
    nil
  end
  
  #get the count of matched profiles
  def self.matched(fb_users, contacts = nil)
    @matched_count = 0
    fb_users.each do |fb_user|
      if contacts
        match_it = self.reverse_proccess(fb_user.user_friend)
      else
        match_it = self.reverse_proccess(fb_user)
      end
      @matched_count += 1 unless match_it.nil?
    end
    @matched_count
  end  
  
  #get the percentage success of matched profiles
  def self.success_rate(users_lin, matched_lin_users = @matched_count)
    rate = matched_lin_users / (users_lin/100.to_f)
    return rate
  end
  
  #get the count of unmatched profiles
  def self.unmatched(lin_users)
    @unmatched_count = User.count_of_users(lin_users) - @matched_count
    return @unmatched_count
  end

  #get if the LinkedIn's profile is matched by FB profile
  def self.reverse_proccess(lin_user)
    fb_users = User.where('linkedin_uid IS NULL')
    fb_users.each do |fb_user|
      return fb_user.facebook_uid if lin_user.linkedin_uid == Match.process(fb_user)
    end
    nil
  end
  
  #returns user's LinkedIn's ID
  def self.process(fb_user, show_page = nil)
    self.check_names(fb_user, show_page)
  end
end