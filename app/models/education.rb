class Education < ActiveRecord::Base
  belongs_to :user

  #method for saving education history of user, who is logged-in
  def self.my_create_new(cur_user)
    case cur_user.provider
      when 'facebook'       
        my_account = FbGraph::User.new(cur_user.facebook_uid, :access_token => cur_user.oauth_token).fetch
        my_account.education.each do |edu|
          self.create({
            :user_id => cur_user.id,
            :school => edu.school.name.downcase,
            :start_year => nil,
            :end_year => nil
          })
        end
        
      when 'linkedin'
        linkedin = LinkedIn::Client.new('ti57m4brxgf2', 'FqPh4FLHr2C0e3gh') 
        linkedin.authorize_from_access(cur_user.oauth_token, cur_user.oauth_secret)
        user = linkedin.profile(:fields => %w(educations))
        user.educations.all.each do |edu|
          self.create({
            :user_id => cur_user.id,
            :school => edu.school_name.downcase,
            :start_year => edu.start_date.year,
            :end_year => edu.end_date.year
          })
        end        
    end
  end
end
