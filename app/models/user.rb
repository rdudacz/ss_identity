class User < ActiveRecord::Base
  has_many :friends_connections_mine, :class_name  => "FriendsConnection", :foreign_key => :user_id
  has_many :friends_connections, :class_name  => "FriendsConnection", :foreign_key => :friend_with
  
  has_many :work_positions
  has_many :educations
    
  acts_as_authentic do |c|
    c.validate_login_field = false
    c.validate_email_field = false
    c.validate_password_field = false
    c.logged_in_timeout(15.minutes)
  end

  #create profile of the linkedin's friend
  def self.create_lin_friend(cur_user, lin_friend)
    user = self.create({:provider => cur_user.provider,
                        :linkedin_uid => lin_friend.id,
                        :username => (lin_friend.public_profile_url.split('/')[4].downcase if lin_friend.public_profile_url),
                        :name => lin_friend.first_name+' '+lin_friend.last_name,
                        :first_name => lin_friend.first_name,
                        :last_name => lin_friend.last_name,
                        :birth => (self.linkedin_birth(lin_friend.date_of_birth) if lin_friend.date_of_birth),
                        :location => lin_friend.main_address
    })
    return user
  end

  #create profile of the facebook's friend
  def self.create_fb_friend(cur_user, fb_friend)
    save_it = self.create({:provider => cur_user.provider,
                           :facebook_uid => fb_friend.identifier,
                           :username => fb_friend.username.to_s.downcase,
                           :name => fb_friend.name,
                           :first_name => fb_friend.first_name,
                           :last_name => fb_friend.last_name,
                           :birth => (fb_friend.birthday unless fb_friend.birthday.nil?),
                           :location => (fb_friend.location.name unless fb_friend.location.nil?)
    })
    return save_it
  end


  #get users from respective social site
  def self.find_users(provider)
    find_by_sql('SELECT DISTINCT(users.id), users.* FROM users JOIN friends_connections ON friends_connections.user_id = users.id WHERE '+provider+'_uid IS NOT NULL ORDER BY users.last_name, users.first_name')
  end

  #print out the ID title into profile page
  def self.display_friends_header(user)
    provider_uid = user.provider + ' | '
    provider_uid += self.display_uid(user)
  end

  #get count of returned rows
  def self.count_of_users(users)
    users.count
  end

  #return UID of respective social site
  def self.display_uid(user)
    return (user.facebook_uid.nil?) ? user.linkedin_uid : user.facebook_uid
  end

  #formating linkedin's birthday to the needed shape
  def self.linkedin_birth(b)
    if b.day && b.month && b.year
      b.day = '0'+b.day.to_s if b.day.to_s.length == 1 
      b.month = '0'+b.month.to_s if b.month.to_s.length == 1 
      birthday = b.year.to_s+'-'+b.month.to_s+'-'+b.day.to_s
      
      return birthday
    end
    nil
  end

  #get username from LinkedIn's URL address
  def self.linkedin_get_username(hash_data)
    public_profile_url = hash_data.fetch('extra', {}).fetch('raw_info', {})['publicProfileUrl'] if hash_data.fetch('extra', {}).fetch('raw_info', {})['publicProfileUrl']
    url_name = public_profile_url.split('/')[4] #and return the url name
    self.username_format(url_name)
  end
  
  #get Facebook's username if exist
  def self.facebook_get_username(hash_data)
    url_name = hash_data.fetch('extra', {}).fetch('raw_info', {})['username'] if hash_data.fetch('extra', {}).fetch('raw_info', {})['username']
    self.username_format(url_name.to_s)
  end
  
  #get username downcases for matching
  def self.username_format(username)
    username.downcase
  end
  
  #check if the user is already in database (=> then log-in him) or not (the make a 'registration')
  def self.find_or_create_from_oauth(auth_hash)
    provider = auth_hash["provider"]
    uid = auth_hash["uid"]
   
    case provider
      when 'facebook'
        if user = self.where('email = ? and provider = ?', auth_hash["info"]["email"], provider).first    
          return user
        elsif user = self.find_by_facebook_uid(uid)
          return user
        else
          return self.create_user_from_facebook(auth_hash)
        end

      when 'linkedin'
        if user = self.find_by_linkedin_uid(uid)        
          return user
        else
          return self.create_user_from_linkedin(auth_hash)
        end
    end
  end

  #saving LinkedIn's informations from API
  def self.create_user_from_linkedin(auth_hash)
    self.create({:provider => auth_hash["provider"],
                 :linkedin_uid => auth_hash["uid"],
                 :username => self.linkedin_get_username(auth_hash),
                 :name => auth_hash["info"]["name"],
                 :first_name => auth_hash["info"]["first_name"],
                 :last_name => auth_hash["info"]["last_name"],
                 :oauth_secret => auth_hash['credentials']['secret'],
                 :oauth_token => auth_hash['credentials']['token']
    })

  end

  #saving Facebook's informations from API
  def self.create_user_from_facebook(auth_hash)
    self.create({:provider => auth_hash["provider"],
                 :facebook_uid => auth_hash["uid"],
                 :username => self.facebook_get_username(auth_hash),
                 :name => auth_hash["info"]["name"],
                 :first_name => auth_hash["info"]["first_name"],
                 :last_name => auth_hash["info"]["last_name"],
                 :birth => (Date.strptime(auth_hash.fetch('extra', {}).fetch('raw_info', {})['birthday'],'%m/%d/%Y') if auth_hash.fetch('extra', {}).fetch('raw_info', {})['birthday']),
                 :location => auth_hash["info"]["location"],
                 :email => auth_hash["info"]["email"],
                 :oauth_secret => auth_hash['credentials']['secret'],
                 :oauth_token => auth_hash['credentials']['token']
    })
  end


  def update_user_from_facebook(auth_hash)
    self.update_attributes({
      :facebook_uid => auth_hash["uid"]
    })
  end
end