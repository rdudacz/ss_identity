class UserSession < Authlogic::Session::Base
  generalize_credentials_error_messages "Login info is invalid!"  
   
  def to_key
     new_record? ? nil : [ self.send(self.class.primary_key) ]
  end
  
  self.logout_on_timeout = true  
  
  def persisted?
    false
  end                
end