module EducationsHelper
  #print out the list of user's schools
  def print_schools(user)
    if user.educations.count > 0
      display_edu = '<ul>'
      user.educations.each do |edu|
        display_edu += content_tag(:li, edu.school)
      end
      display_edu += '</ul>'
    else
      display_edu = '-'
    end
    display_edu.html_safe
  end
end
