module UsersHelper
  #print out in Facebook's table matched LinkedIn's ID
  def matched_with(matched_user, matched_by_user)
    unless matched_user.nil? && matched_user.blank?
      user = User.find_by_linkedin_uid(matched_user)
      link_to(user.linkedin_uid, user_path(user, :matched_by => matched_by_user.id))
    end
  end

  #print out in LinkedIn's table matched Facebook's ID
  def matched_by(matched_by_user, matched_user, show = nil)
    unless matched_by_user.nil? && matched_by_user.blank?
      user = User.find_by_facebook_uid(matched_by_user)
      
      unless show.nil?
        link_to('Show', user_path(matched_user.id, :matched_by => user))
      else
        link_to(user.facebook_uid, user)
      end
    else
      link_to('Show', user_path(matched_user.id, :just_display => true)) unless show.nil?
    end
  end

  #print out the provider in its color
  def provider_color(user)
    if user.provider == 'facebook'
      content_tag(:span, user.provider, :class => 'facebook')
    else
      content_tag(:span, user.provider, :class => 'linkedin')
    end 
  end
  
  #print out the kind of matches
  def matched_info(user)
    if user.facebook_uid.nil?
      return 'Matched with this LinkedIn profile'
    else
      return 'Matched by this Facebook profile'
    end
  end
  
  #preparing associations for matching contacts (friends)
  def display_matching_info(person, friend)
    if person.provider == 'linkedin'
      match_process = Match.reverse_proccess(friend.user_friend)
      return matched_by(match_process, friend)
    else
      return matched_with(Match.process(friend.user_friend), friend.user_friend)
    end
  end
  
  #initializing of users from social sites
  def stats_provider(usr, mtch)
    if usr.provider == 'linkedin'
      @lin = usr.friends_connections_mine
    else 
      @lin = mtch.friends_connections_mine
    end
    
    if mtch.provider == 'facebook'
      @fb = mtch.friends_connections_mine
    else 
      @fb = usr.friends_connections_mine
    end
  end
  
end
