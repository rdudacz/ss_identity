module ApplicationHelper
  #print out the header of the app
  def display_header
    content_tag(:header) do
      content_tag(:div, content_tag(:h1, 'Social Sites Identity'), :class => 'page-header')
    end
  end
  
  #print out the box with currently logged-in user
  def logged_in_box
      logged_in = '<div class="well">'
        #logged_in += 'Logged-in as <strong>'+(link_to current_user.name, current_user)+'</strong> via <strong>'+current_user.provider+'</strong> | '
        logged_in += 'Logged-in as <strong>'+current_user.name+'</strong> via <strong>'+current_user.provider+'</strong> | '
        logged_in += link_to 'Log out', logout_path
      logged_in += '</div>'  
      logged_in.html_safe
  end

  #print out the homepage for unknown user
  def log_in_as
    log_in = '<p>'
      log_in += '<h3>Log-in via:</h3>'
      log_in += link_to 'Facebook', fb_login_path
      log_in += ' or '
      log_in += link_to 'LinkedIn', linkedin_login_path
    log_in += '</p>'  
    log_in.html_safe
  end
  
  #print out the footer
  def display_footer
    display = content_tag(:footer, '', :class => 'footer') do
      content_tag(:p, ('Copyright 2012, ' + content_tag(:a, 'Radek Duda', :href => 'mailto:rdudacz@gmail.com')).html_safe) 
    end
  end
  
  #display formated rate of matching
  def display_matched_rate(number, total, rating)
    matched_line = number.to_s+' of '+total.to_s+' (<strong>'+rating.to_s+'%</strong>)'
    return matched_line.html_safe
  end
end
