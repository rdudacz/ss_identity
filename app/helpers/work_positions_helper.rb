module WorkPositionsHelper
  #print out user's work history
  def print_work_positions(user)
    wp = WorkPosition.where('user_id = ? AND friends_connection_uid IS NULL', user.id)
    if wp.count > 0
      display_wp = '<ul>'
      wp.each do |work|
        display_wp += content_tag(:li, work.position)
      end
      display_wp += '</ul>'
    else
      display_wp = '-'
    end
    display_wp.html_safe
  end
end
