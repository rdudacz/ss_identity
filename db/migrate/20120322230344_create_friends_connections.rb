class CreateFriendsConnections < ActiveRecord::Migration
  def change
    create_table :friends_connections do |t|
      t.integer :user_id
      t.integer :friend_with

      t.timestamps
    end
  end
end
