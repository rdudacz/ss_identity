class EducationAddAndFixColumn < ActiveRecord::Migration
  def up
    rename_column :educations, :year, :start_year
    add_column :educations, :end_year, :integer
  end

  def down
  end
end
