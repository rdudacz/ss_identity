class RemoveUidToFriendsConnection < ActiveRecord::Migration
  def up
    remove_column :friends_connections, :friends_connection_uid
      end

  def down
    add_column :friends_connections, :friends_connection_uid, :string
  end
end
