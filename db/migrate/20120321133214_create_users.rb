class CreateUsers < ActiveRecord::Migration
  def self.up
     create_table :users do |t|
       t.string    :name,                :null => false   
       t.string    :username,            :null => false                             
       t.string    :first_name,          :null => false                
       t.string    :last_name,           :null => false                
       t.string    :birth                            
       t.string    :location                            
       t.string    :crypted_password,    :default => nil, :null => true             
       t.string    :password_salt,       :default => nil, :null => true          
       t.string    :email,               :default => nil, :null => true          
       t.string    :persistence_token,   :null => false      
       t.string    :single_access_token, :null => false              
       t.string    :perishable_token,    :null => false            

       t.integer   :login_count,         :null => false, :default => 0 
       t.integer   :failed_login_count,  :null => false, :default => 0 
       t.datetime  :last_request_at                                    
       t.datetime  :current_login_at                                   
       t.datetime  :last_login_at                                      
       t.string    :current_login_ip                                   
       t.string    :last_login_ip                                      

       t.string    :oauth_token
       t.string    :oauth_secret
       t.string    :facebook_uid
       t.string    :linkedin_uid
       t.string    :provider
       
       t.timestamps
     end
   end

   def self.down
     drop_table :users
   end
end
