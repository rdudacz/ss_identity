class CreateWorkPositions < ActiveRecord::Migration
  def change
    create_table :work_positions do |t|
      t.integer :user_id
      t.string :position

      t.timestamps
    end
  end
end
